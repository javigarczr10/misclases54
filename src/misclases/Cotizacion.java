/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author safoe
 */
public class Cotizacion {
    private int numCotizacion;
    private String descripcion;
    private float precio;
    private float porcentajePagoInicial;
    private int plazo;



    public Cotizacion(){
        this.numCotizacion =0;
        this.descripcion ="";
        this.precio=0.0f;
        this.porcentajePagoInicial=0.0f;
        this.plazo=0;

  



    }
    public Cotizacion(int numCotizacion,String descripcion,float precio, float porcentajePagoInicial, int plazo){
        this.numCotizacion = numCotizacion;
        this.descripcion = descripcion;
        this.precio= precio;
        this.porcentajePagoInicial= porcentajePagoInicial;
        this.plazo= plazo;
}
    public Cotizacion(Cotizacion otro){
        this.numCotizacion = otro.numCotizacion;
        this.descripcion = otro.descripcion;
        this.precio= otro.precio;
        this.porcentajePagoInicial= otro.porcentajePagoInicial;
        this.plazo= otro.plazo;
}

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    public float calcularPagoInicial(){
        float pagoInicial=0.0F;
        pagoInicial=this.precio*this.porcentajePagoInicial /100;
        return pagoInicial;
    }
    public float calcularTotalFinanciar(){
        float totalFinanciar=0.0f;
        totalFinanciar=this.precio-this.calcularPagoInicial();
        return totalFinanciar;
    }
    public float calcularPagoMensual(){
        float pagoMensual=0.0f;
        pagoMensual=this.calcularTotalFinanciar()/this.plazo;
        return pagoMensual;
    }
    public void imprimirCotizacion (){
        System.out.println("numero de cotizacion :" + this.numCotizacion);
        System.out.println("descripcion :" + this.descripcion);
        System.out.println("precio :" + this.precio);
        System.out.println("porcentaje de pago inicial :" + this.porcentajePagoInicial);
        System.out.println("plazo :" + this.plazo);
        System.out.println("su pago inicial sera:" + this.calcularPagoInicial());
        System.out.println("su total a financiar es:" + this.calcularTotalFinanciar());
        System.out.println("su pago mensual es: " + this.calcularPagoMensual());
    }
    
    
    
    
    
}
    
    
    
    
    
    
    
    
    
    
