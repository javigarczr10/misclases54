/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author safoe
 */
public class Factura {
    private int numFactura;
    private String rfc;
    private String nombreCliente;
    private String domFiscal;
    private String descripcion;
    private String fechaVentas;
    private float totalVentas;
    
    //metodos
    //metodos constructores

    public Factura() {
        this.numFactura =0;
        this.rfc = "";
        this.nombreCliente = "";
        this.domFiscal = "";
        this.descripcion = "";
        this.fechaVentas = "";
        this.totalVentas = 0.0f;
        
    }

    public Factura(int numFactura, String rfc, String nombreCliente, String domFiscal, String descripcion, String fechaVentas, float totalVentas) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domFiscal = domFiscal;
        this.descripcion = descripcion;
        this.fechaVentas = fechaVentas;
        this.totalVentas = totalVentas;
    }
    
    public Factura(Factura otro){
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nombreCliente = otro.nombreCliente;
        this.domFiscal = otro.domFiscal;
        this.descripcion = otro.descripcion;
        this.fechaVentas = otro.fechaVentas;
        this.totalVentas = otro.totalVentas;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomFiscal() {
        return domFiscal;
    }

    public void setDomFiscal(String domFiscal) {
        this.domFiscal = domFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVentas() {
        return fechaVentas;
    }

    public void setFechaVentas(String fechaVentas) {
        this.fechaVentas = fechaVentas;
    }

    public float getTotalVentas() {
        return totalVentas;
    }

    public void setTotalVentas(float totalVentas) {
        this.totalVentas = totalVentas;
    }
    
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto= this.totalVentas * .16f;
        return impuesto; 
    }
    
    public float calcularTotalPagar(){
        float total= 0.0f;
        total= this.totalVentas + this.calcularImpuesto();
        return total;
    }
    public void imprimirFactura(){
        System.out.println("numero de factura :" + this.numFactura);
        System.out.println("RFC :" + this.rfc);
        System.out.println("nombre del cliente :" + this.nombreCliente);
        System.out.println("domicilio fical :" + this.domFiscal);
        System.out.println("descripcion :" + this.descripcion);
        System.out.println("fecha de venta :" + this.fechaVentas);
        System.out.println("total de venta" + this.totalVentas);
        System.out.println("impuesto" + this.calcularImpuesto());
        System.out.println("total pagar" + this.calcularTotalPagar());
    }
    
    
}//termina la clase
}

